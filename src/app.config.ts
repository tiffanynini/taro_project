export default defineAppConfig({
  // 页面路径列表
  pages: [
    'pages/index/index',
    'pages/home/index',
    'pages/detail/index'
  ],
  // 全局的默认窗口表现
  window: {
    backgroundTextStyle: 'light',
    // 导航栏背景颜色
    navigationBarBackgroundColor: '#F8C01E',
    navigationBarTitleText: 'home',
    navigationBarTextStyle: 'black',
    // 是否开启当前页面的下拉刷新功能
    enablePullDownRefresh:true
  },
  // tabBar 底部tab栏的表现
  tabBar: {
    color: "#d0d3db",
    selectedColor: "#333",
    backgroundColor: "#fff",
    borderStyle: "white",
    list: [
      {
        pagePath: "pages/index/index",
        iconPath: "./assets/tabBar/home.png",
        selectedIconPath: "./assets/tabBar/home-active.png",
        text: "首页"
      },
      {
        pagePath: "pages/home/index",
        iconPath: "./assets/tabBar/user.png",
        selectedIconPath: "./assets/tabBar/user-active.png",
        text: "我的"
      }
    ]
  },
  // 小程序接口权限相关设置
  permission:{
    // 位置相关权限声明
    'scope.userLocation':{
      desc:'你的位置信息将用于小程序位置接口的效果展示'
    }
  },
  // 后台运行的能力 audio 后台音乐播放 location 后台定位
  requiredBackgroundModes:['audio','location'],
  // 小程序的默认启动路径（首页），不填则默认为pages列表的第一项
  entryPagePath:'pages/index/index'
  // subPackages 分包结构配置
})
