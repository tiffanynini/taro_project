export default definePageConfig({
  navigationBarTitleText: '我的',
  // 是否启用分享给好友
  enableShareAppMessage:true,
  // 是否启用分享到朋友圈
  enableShareTimeline:true
})
