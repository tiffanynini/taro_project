export default definePageConfig({
  navigationBarTitleText: '商品详情',
  // 是否启用分享给好友
  enableShareAppMessage:true,
  // 是否启用分享到朋友圈
  enableShareTimeline:true
})
