import { View, Image } from '@tarojs/components'
import { useLoad } from '@tarojs/taro'
import SwiperOne from "@/assets/Images/home_swiper1.jpg";
import './index.scss'

export default function Home() {

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View>
      <Image src={SwiperOne}></Image>
    </View>
  )
}
