import { View, Text, Swiper, SwiperItem, Image } from "@tarojs/components";
import {
  useDidHide,
  useDidShow,
  useLoad,
  usePullDownRefresh,
  useReady,
} from "@tarojs/taro";
import SwiperOne from "@/assets/Images/home_swiper1.jpg";
import SwiperTwo from "@/assets/Images/home_swiper2.jpg";
import SwiperThree from "@/assets/Images/home_swiper3.jpg";
import SwiperFour from "@/assets/Images/home_swiper4.jpg";

import "./index.scss";
import { useEffect, useState } from "react";
import Taro from "@tarojs/taro";
import { log } from "console";
// 初始化数据
const swiperImgs = [
  {
    id: 1,
    imgUrl: SwiperOne,
  },
  {
    id: 2,
    imgUrl: SwiperTwo,
  },
  {
    id: 3,
    imgUrl: SwiperThree,
  },
  {
    id: 4,
    imgUrl: SwiperFour,
  },
];

export default function Index() {
  // 1.内置事件以on开头
  useLoad(() => {
    console.log("Page loaded.");
  });
  const clickHandler = (e) => {
    e.stopPropagation(); // 阻止冒泡
  };

  // 2.可以使用所有的React Hooks
  useEffect(() => {}, []);

  // 对应onReady
  useReady(() => {});

  // 对应onShow
  useDidShow(() => {});

  // 对应onHide
  useDidHide(() => {});

  // taro对所有小程序页面生命周期都实现了对应的自定义React Hooks 进行支持
  usePullDownRefresh(() => {}); //监听用户下拉

  const toHome = () => {
    console.log(11);
    // 路由跳转 跳转到目的页，打开新页面
    Taro.navigateTo({
      url: "../../pages/detail/index",
    });
  };

  return (
    <View className="index">
      {/* 轮播图 */}
      <Swiper
        className="box"
        autoplay
        interval={1000}
        indicatorColor="#999"
        onClick={clickHandler}
        onAnimationFinish={() => {}}
      >
        {swiperImgs?.map((item) => (
          <SwiperItem
            key={item?.id}
            style={{ display: "flex", alignItems: "center" }}
          >
            <Image
              src={item?.imgUrl}
              lazyLoad
              mode="aspectFill"
              onClick={() => toHome()}
            />
          </SwiperItem>
        ))}
      </Swiper>
    </View>
  );
}
